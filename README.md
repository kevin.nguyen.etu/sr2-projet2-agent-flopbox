# SR2-Projet2-Agent-FlopBox

Projet réalisé par Kévin Nguyen et Matthias Severin

## Remarques

La video montre le bon fonctionnement du projet.
Toutes les éxigences du TP ont été remplies.
Il y a juste un probleme dû au fait que sur le serveur FTP, la date de modification est precise a la minute et non a la seconde, donc il faut qu'un fichier soit modifié au moins 1 minutes apres sa derniere mise a jour, sinon, lors de la prochaine mise a jour, cette modification ne pourra pas etre prise en compte.

## Generation de la javadoc

mvn javadoc:javadoc

## Generation du fichier jar

mvn package assembly:single

## Lancement du flopbox

java -jar target/agent-flopbox-1.0-SNAPSHOT-jar-with-dependencies.jar
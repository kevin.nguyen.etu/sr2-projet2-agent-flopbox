package fil.sr2.AGENT_FLOPBOX;

public class AgentFlopboxException extends RuntimeException {
	
	public AgentFlopboxException(String msg) {
		super(msg);
	}

}

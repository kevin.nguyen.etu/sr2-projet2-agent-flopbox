package fil.sr2.AGENT_FLOPBOX;

import java.util.concurrent.TimeUnit;

public class AgentFlopboxMain {
	
    public static void main( String[] args )
    {
        AgentClient agent = new AgentClient();
        agent.post();
        agent.updateAlias();
        agent.createAliasFolder();
        agent.addUser("webtp.fil.univ-lille1.fr", "severin", "matt-4499");
        
        agent.createAllFiles("webtp.fil.univ-lille1.fr", "/");
        while (true) {
        	try {
				TimeUnit.SECONDS.sleep(3);
				System.out.println("Update !");
				agent.updateAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
    }
}

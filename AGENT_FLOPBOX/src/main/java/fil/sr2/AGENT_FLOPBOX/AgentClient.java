package fil.sr2.AGENT_FLOPBOX;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;


public class AgentClient {
	
	private Map<String, User> serverList = new HashMap<String, User>();
	private String pathFolder = "/tmp/TEST_AGENT_FLOPBOX/"; //Absolute path to where create the folders
	private HttpClient client;
	
	public AgentClient () {
		this.client = new HttpClient();
	}
	
	/** 
	 * Methode de test, juste pour ajouter un serveur FTP a la plateforme flopbox
	 */
	public void post() {
		try {
			PostMethod post = new PostMethod("http://localhost:8080/flopbox/webtp.fil.univ-lille1.fr");
			this.client.executeMethod(post);
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to create the alias on flopbox");
		}
	}
	
	/**
	 * Met a jour serverList pour avoir tous les alias.
	 */
	public void updateAlias() {
		try {
			GetMethod get = new GetMethod("http://localhost:8080/flopbox/");
			this.client.executeMethod(get);
			serverList.clear();
			String res = get.getResponseBodyAsString();
			for (String alias : res.split(" ")) {
				serverList.put(alias, null);
			}
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to update the alias list");
		}
	}
	
	/**
	 * Methode pour initialiser les dossiers locaux pour tous les serveurs FTP.
	 */
	public void getAllFile() {
		for (String alias : serverList.keySet()) {
			createAllFiles(alias, "/");
		}
	}
	
	/**
	 * Met a jour tous les serveurs FTP
	 */
	public void updateAll() {
		for (String alias : serverList.keySet()) {
			updateFromLocal(alias, "/");
			updateFromFTP(alias, "/");
		}
	}
	
	/** Recupere les informations du dossier
	 * @param alias l'alias du serveur FTP
	 * @param path Le chemin
	 * @return La liste d'informations
	 */
	public String listDirectory(String alias, String path) {
		try {
			GetMethod get = new GetMethod("http://localhost:8080/flopbox/" + alias + path);
			User user = serverList.get(alias);
			if (user != null) {
				get.setRequestHeader("username", user.username);
				get.setRequestHeader("password", user.password);
			}
			this.client.executeMethod(get);
			return get.getResponseBodyAsString();
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to list the directory : " + path);
		}
	}
	
	/** Methode pour initialiser les dossiers locaux d'un serveurs FTP.
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin a creer
	 */
	public void createAllFiles(String alias, String path) {
		String list = listDirectory(alias, path);
		if (list.length() == 0) return; // empty directory
		
		for (String filedata : list.split("\n")) {
			String[] split = filedata.split(" ");
			String filename = split[split.length - 1];
			if (filedata.charAt(0) == '-') {
				createFile(alias, path + filename);
			} else if (filedata.charAt(0) == 'd') {
				Path path2 = Paths.get(pathFolder + alias + path + filename);
				try {
					Files.createDirectories(path2);
					createAllFiles(alias, path + filename + "/");
				} catch (IOException e) {
					throw new AgentFlopboxException("Failed to create the directory : " + path2.getFileName());
				}
			}
		}
	}
	
	/** Copie un fichier du serveur FTP en local
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin du fichier
	 */
	public void createFile(String alias, String path) {
		GetMethod get = new GetMethod("http://localhost:8080/flopbox/" + alias + "/file" + path);
		User user = serverList.get(alias);
		if (user != null) {
			get.setRequestHeader("username", user.username);
			get.setRequestHeader("password", user.password);
		}
		try {
			this.client.executeMethod(get);
			File file = new File(pathFolder + alias + path);
			file.createNewFile();
			byte[] bytes = get.getResponseBody();
			FileOutputStream outputStream = new FileOutputStream(file);
			outputStream.write(bytes);
			outputStream.close();
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to copie distant file");
		}
	}
	
	/** Copie un fichier local sur le serveur FTP
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin du fichier
	 * @param file Le fichier a copier
	 */
	public void uploadFile(String alias, String path, File file) {
		PostMethod post = new PostMethod("http://localhost:8080/flopbox/" + alias + "/file" + path);
		User user = serverList.get(alias);
		if (user != null) {
			post.setRequestHeader("username", user.username);
			post.setRequestHeader("password", user.password);
		}
		try {
			FilePart fp = new FilePart("file", file);
			Part[] parts = { fp };
			post.setRequestEntity(new MultipartRequestEntity(parts, post.getParams()));
			this.client.executeMethod(post);
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to upload file");
		}
	}
	
	/** Creer un dossier sur le serveur FTP
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin du dossier
	 */
	public void createDistantDirectory(String alias, String path) {
		PostMethod post = new PostMethod("http://localhost:8080/flopbox/" + alias + "/directory" + path);
		User user = serverList.get(alias);
		if (user != null) {
			post.setRequestHeader("username", user.username);
			post.setRequestHeader("password", user.password);
		}
		try {
			this.client.executeMethod(post);
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to create distant directory");
		}
	}
	
	/** Supprimer un fichier du serveur FTP (le place dans le dossier .deleted)
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin du fichier
	 * @param filename Le nom du fichier
	 */
	public void deleteDistantFile(String alias, String path, String filename) {
		PutMethod put = new PutMethod("http://localhost:8080/flopbox/" + alias + "/file" + path + filename);
		User user = serverList.get(alias);
		if (user != null) {
			put.setRequestHeader("username", user.username);
			put.setRequestHeader("password", user.password);
		}
		put.setRequestHeader("rename", "/.deleted/" + filename);
		try {
			this.client.executeMethod(put);
		} catch (IOException e) {
			throw new AgentFlopboxException("Failed to delete distant file");
		}
	}
	
	/** Met a jour les fichiers par rapport fichier locaux
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin a mettre a jour
	 */
	public void updateFromLocal(String alias, String path) {
		File directory = new File(pathFolder + alias + path);
		File[] files = directory.listFiles();
		String[] filedatas = listDirectory(alias, path).split("\n");
		SimpleDateFormat formater = new SimpleDateFormat("MMM dd yyyy HH:mm");
		for (File file : files) {
			String filename = file.getName();
			String filedata = null;
			for (int i = 0; i < filedatas.length; i++) {
				if (filedatas[i].endsWith(filename)) {
					filedata = filedatas[i];
					break;
				}
			}
			if (filedata == null) {
				if (file.isFile()) {
					uploadFile(alias, path + filename, file);
				} else if (file.isDirectory()) {
					createDistantDirectory(alias, path + filename);
					updateFromLocal(alias, path + filename + "/");
				}
			} else {
				try {
					Date datelocal = new Date(file.lastModified());
					Date dateftp = formater.parse(filedata.substring(43, 49) + " 2021 " + filedata.substring(50, 55));
					long diff = dateftp.getTime() - datelocal.getTime();
					if (diff < -60000) { // 1 minute de difference, a cause du manque de precision du serveur FTP
						if (file.isFile()) {
							uploadFile(alias, path + filename, file);
						}
						else {
							updateFromLocal(alias, path + filename + "/");
						}
					}
				} catch (ParseException e) {
					throw new AgentFlopboxException("Failed to update file");
				}
			}
		}
	}
	
	/** Met a jour les fichiers par rapport au serveur FTP
	 * @param alias L'alias du serveur FTP
	 * @param path Le chemin a mettre a jour
	 */
	public void updateFromFTP(String alias, String path) {
		String list = listDirectory(alias, path);
		if (list.length() == 0) return; // empty directory
		
		SimpleDateFormat formater = new SimpleDateFormat("MMM dd yyyy HH:mm");
		for (String filedata : list.split("\n")) {
			String[] split = filedata.split(" ");
			String filename = split[split.length - 1];
			if (filedata.charAt(0) == '-') {
				File file = new File(pathFolder + alias + path + filename);
				if (file.exists()) {
					try {
						Date datelocal = new Date(file.lastModified());
						Date dateftp = formater.parse(filedata.substring(43, 49) + " 2021 " + filedata.substring(50, 55));
						long diff = dateftp.getTime() - datelocal.getTime();
						if (diff > 60000) {
							createFile(alias, path + filename);
						}
					} catch (ParseException e) {
						throw new AgentFlopboxException("Failed to update file");
					}
				} else {
					deleteDistantFile(alias, path, filename);
				}
			} else if (filedata.charAt(0) == 'd') {
				File file = new File(pathFolder + alias + path + filename);
				if (! file.exists()) {
					deleteDistantFile(alias, path, filename);
				}
				updateFromFTP(alias, path + filename + "/");
			}
		}
	}
	
	public void addUser(String alias, String username, String password) {
		if (serverList.keySet().contains(alias)) {
			serverList.put(alias, new User(username, password));
		}
	}
	
	public void removeUser(String alias) {
		if (serverList.keySet().contains(alias)) {
			serverList.put(alias, null);
		}
	}
	
	public Set<String> getServerList() {
		return this.serverList.keySet();
	}
	
	public void addAlias(String alias) {
		if (serverList.keySet().contains(alias)) {
			System.out.println("Alias déjà dans la liste : " + alias);
		}
		else {
			serverList.put(alias, null);
		}
	}
	
	public void removeAlias(String alias) {
		if (!serverList.keySet().contains(alias)) {
			System.out.println("Alias non existant dans la liste : " + alias);
		}
		else {
			serverList.remove(alias);
		}
	}
	
	public void createAliasFolder() {
		try {
			for (String alias : serverList.keySet()) {
				Path path = Paths.get(pathFolder + alias);
				Files.createDirectories(path);
			}
			
		} catch (IOException e) {
			throw new AgentFlopboxException("Impossible to create folders for FTP servers.");
		}
	}
	
	public void removeAliasFolder(String folderName) {
		Path path = Paths.get("/tmp/TEST_AGENT_FLOPBOX/" + folderName);
		try {
			Files.delete(path);
		} 
		catch (IOException e) {
			throw new AgentFlopboxException("Error folder do seems to not exist.");
		}
	}
	
	
}
